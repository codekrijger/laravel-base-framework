<?php

use App\Authentication\Controllers\EmailVerification;
use App\Authentication\Controllers\EmailVerificationResend;
use App\Authentication\Controllers\ForgotPassword;
use App\Authentication\Controllers\Logout;
use App\Authentication\Controllers\Registration;
use App\Authentication\Controllers\RequestToken;
use App\Authentication\Controllers\ResetPassword;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [RequestToken::class, 'post'])
    ->middleware(['guest'])
    ->name('login');

Route::post('logout', Logout::class)
    ->middleware(['auth:sanctum'])
    ->name('logout');

Route::post('register', [Registration::class, 'post'])
    ->middleware(['guest'])
    ->name('register');

Route::post('forgot-password', [ForgotPassword::class, 'post'])
    ->middleware(['guest'])
    ->name('password.forgot');

Route::post('reset-password', [ResetPassword::class, 'post'])
    ->middleware(['guest'])
    ->name('password.reset');

Route::get('/email/verify/{id}/{hash}', EmailVerification::class)
    ->middleware(['auth:sanctum', 'signed', 'throttle:6,1'])
    ->name('verification.verify');

Route::post('/email/verification/resend', EmailVerificationResend::class)
    ->middleware(['auth:sanctum', 'throttle:6,1'])
    ->name('verification.send');
