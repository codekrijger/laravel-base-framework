const colors = require('tailwindcss/colors')

module.exports = {
    purge: [
        './resources/**/*.blade.php',
        './resources/**/*.js',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                primary: {
                    "100": "#f7f2fc",
                    "200": "#e8ddf8",
                    "300": "#dac8f3",
                    "400": "#ccb3ef",
                    "500": "#be9eea",
                    "600": "#8e57db",
                    "700": "#6227b5",
                    "800": "#3d1871",
                    "900": "#17092a"
                },
                secondary: colors.white,
                neutral: colors.gray,
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
    ],
}
