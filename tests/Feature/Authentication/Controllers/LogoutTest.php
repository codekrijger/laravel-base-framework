<?php

namespace Authentication\Controllers;

use App\Models\User;
use Illuminate\Auth\Authenticatable;
use Tests\TestCase;

/**
 * Tests the working of the {@see Logout} controller.
 */
class LogoutTest extends TestCase
{
    /**
     * Asserts that we can't invoke the logout controller if no user is logged in.
     */
    public function test_it_does_not_allow_logging_out_if_not_logged_in()
    {
        $response = $this->post(route('logout'));

        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * Asserts that the logout controller successfully removes the current token.
     */
    public function test_it_deletes_the_associated_token_from_the_database()
    {
        /** @var User|Authenticatable $user */
        $user = User::factory()->create();
        $token = $user->createToken('unit-test', ['*']);

        $tokenCountBeforeLogout = $user->tokens()->count();

        $this->assertEquals(1, $tokenCountBeforeLogout, 'We should have created a token.');

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $token->plainTextToken)
            ->post(route('logout'));
        $response->assertStatus(200);
        $response->assertJson(['status' => __('auth.logged-out')]);

        $tokenCountAfterLogout = $user->tokens()->count();
        $this->assertEquals(0, $tokenCountAfterLogout, 'The token should have been deleted after logging out.');
    }

    /**
     * This unit test verifies that we're only deleting the current token and not all tokens
     * upon invoking the logout controller.
     */
    public function test_it_only_deletes_the_current_token()
    {
        /** @var User|Authenticatable $user */
        $user = User::factory()->create();
        $token1 = $user->createToken('unit-test-1', ['*']);
        $token2 = $user->createToken('unit-test-2', ['*']);

        $tokenCountBeforeLogout = $user->tokens()->count();

        $this->assertEquals(2, $tokenCountBeforeLogout, 'We should have created 2 tokens.');

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $token1->plainTextToken)
            ->post(route('logout'));
        $response->assertStatus(200);
        $response->assertJson(['status' => __('auth.logged-out')]);

        $tokenCountAfterLogout = $user->tokens()->count();
        $this->assertEquals(1, $tokenCountAfterLogout, 'Only 1 token should have been deleted.');
    }
}
