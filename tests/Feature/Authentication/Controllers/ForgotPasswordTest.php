<?php

namespace Authentication\Controllers;

use App\Authentication\Controllers\ForgotPassword;
use App\Models\User;
use Faker\Factory;
use Illuminate\Support\Facades\Password;
use Tests\TestCase;

/**
 * This class tests the working of the {@see ForgotPassword} controller.
 */
class ForgotPasswordTest extends TestCase
{
    /**
     * Asserts that resets are only accepted for existing users.
     */
    public function test_it_should_not_create_password_reset_tokens_for_non_existing_emails()
    {
        $faker = Factory::create();

        $this->json('post', route('password.forgot'), [
            'email' => $faker->email,
        ])
            ->assertStatus(400)
            ->assertJson(['errors' => [__(Password::INVALID_USER)]]);
    }

    /**
     * Asserts that it blocks requests with an invalid payload.
     */
    public function test_it_blocks_requests_with_an_invalid_payload()
    {
        $this->json('post', route('password.forgot'), [])
            ->assertStatus(400)
            ->assertJsonStructure(['email' => []]);
    }

    /**
     * Asserts that it successfully sends emails for existing users.
     */
    public function test_it_successfully_sends_the_email_for_existing_users()
    {
        $user = User::factory()->create();

        $this->json('post', route('password.forgot'), [
            'email' => $user->email,
        ])
            ->assertStatus(200)
            ->assertJson(['status' => __(Password::RESET_LINK_SENT)]);
    }

    /**
     * Asserts that there is a rate limiter in place to prevent spammers.
     */
    public function test_it_rate_limits_users_who_request_multiple_password_resets()
    {
        $user = User::factory()->create();

        for ($i = 0; $i < 5; $i++) {
            $this->json('post', route('password.forgot'), [
                'email' => $user->email,
            ]);
        }

        $this->json('post', route('password.forgot'), [
            'email' => $user->email,
        ])
            ->assertStatus(400)
            ->assertJson(['errors' => [__(Password::RESET_THROTTLED)]]);
    }
}
