<?php

namespace Authentication\Controllers;

use App\Authentication\Controllers\ResetPassword;
use App\Models\User;
use Faker\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Tests\TestCase;

/**
 * This class tests the working of the {@see ResetPassword} controller.
 */
class ResetPasswordTest extends TestCase
{
    /**
     * Asserts that it blocks requests with an invalid payload.
     */
    public function test_it_blocks_requests_with_an_invalid_payload()
    {
        $this->json('post', route('password.reset'), [])
            ->assertStatus(400)
            ->assertJsonStructure(['token' => [], 'email' => [], 'password' => []]);
    }

    /**
     * Asserts that passwords that are too short are also being blocked.
     */
    public function test_it_does_not_allow_passwords_that_are_too_short()
    {
        $this->json('post', route('password.reset'), [
            'password' => '1234567'
        ])
            ->assertStatus(400)
            ->assertJsonStructure(['token' => [], 'email' => [], 'password' => []]);
    }

    /**
     * Asserts that requests with an invalid token are being blocked.
     */
    public function test_it_blocks_requests_with_an_invalid_token()
    {
        $user = User::factory()->create();

        $this->json('post', route('password.reset'), [
            'email' => $user->email,
            'password' => 'new-password',
            'token' => 'faulty-token',
        ])
            ->assertStatus(400)
            ->assertJson(['errors' => [__(Password::INVALID_TOKEN)]]);
    }

    /**
     * Asserts that the code properly handles emails that are not known to the system
     * (thus non-existing users).
     */
    public function test_it_successfully_errors_for_unknown_emails()
    {
        $faker = Factory::create();

        $this->json('post', route('password.reset'), [
            'email' => $faker->email,
            'password' => 'new-password',
            'token' => 'faulty-token',
        ])
            ->assertStatus(400)
            ->assertJson(['errors' => [__(Password::INVALID_USER)]]);
    }

    /**
     * Asserts that it successfully updates the password of the user upon a correct
     * token entry.
     */
    public function test_it_successfully_updates_users_on_valid_requests()
    {
        $user = User::factory()->create();
        $token = Password::createToken($user);
        $newPassword = 'new-password';

        $this->assertFalse(Auth::attempt(['email' => $user->email, 'password' => $newPassword]));

        $this->json('post', route('password.reset'), [
            'email' => $user->email,
            'password' => $newPassword,
            'token' => $token,
        ])
            ->assertStatus(200)
            ->assertJson(['status' => __(Password::PASSWORD_RESET)]);

        $this->assertTrue(Auth::attempt(['email' => $user->email, 'password' => $newPassword]));
    }
}
