<?php

namespace Authentication\Controllers;

use App\Authentication\Controllers\Registration;
use App\Models\User;
use Faker\Factory;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Tests the working of the {@see Registration} controller.
 */
class RegistrationTest extends TestCase
{
    /**
     * Tests the happy flow of the registration controller.
     */
    public function test_it_successfully_registers_a_new_valid_user()
    {
        Event::fake();
        $faker = Factory::create();
        $email = $faker->email;
        $password = $faker->password(8, 20);

        $response = $this->post(route('register'), [
            'name' => $faker->name,
            'email' => $email,
            'password' => $password,
            'device_name' => $faker->userAgent
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['status', 'token', 'is_verified']);
        Event::assertDispatched(Registered::class);

        $this->assertTrue(
            Auth::attempt(['email' => $email, 'password' => $password]),
            'The user should be able to login after registration.'
        );
    }

    /**
     * Tests whether or not our unique email constraint is working.
     */
    public function test_it_prevents_re_registration_of_an_already_in_use_email_address()
    {
        $faker = Factory::create();
        $user = User::factory()->create();

        $this->json('post', route('register'), [
            'name' => $faker->name,
            'email' => $user->email,
            'password' => $faker->password,
            'device_name' => $faker->userAgent
        ])
            ->assertStatus(400)
            ->assertJsonStructure(['email' => []]);
    }

    /**
     * Tests whether or not invalid passwords are being blocked.
     */
    public function test_it_blocks_passwords_being_too_short()
    {
        $faker = Factory::create();

        $this->json('post', route('register'), [
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => 'short',
            'device_name' => $faker->userAgent
        ])
            ->assertStatus(400)
            ->assertJsonStructure(['password' => []]);
    }
}
