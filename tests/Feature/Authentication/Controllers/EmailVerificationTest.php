<?php

namespace Authentication\Controllers;

use App\Authentication\Controllers\EmailVerification;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\URL;
use Tests\TestCase;

/**
 * Tests the working of the {@see EmailVerification} controller.
 */
class EmailVerificationTest extends TestCase
{
    /**
     * Asserts that the code does not verify the user if the user is not logged in.
     * Instead it should trigger the {@see \App\Http\Middleware\Authenticate} middleware.
     */
    public function test_it_does_not_verify_users_if_they_are_not_logged_in()
    {
        $verificationUrl = URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(60),
            ['id' => 1, 'hash' => sha1('wrong-email')]
        );

        $response = $this->get($verificationUrl);

        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * Asserts that email verification is not done for cases where the parameters
     * are invalid.
     */
    public function test_it_does_not_verify_the_user_with_invalid_request_data()
    {
        $user = User::factory()->create([
            'email_verified_at' => null,
        ]);

        $verificationUrl = URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(60),
            ['id' => $user->id, 'hash' => sha1('wrong-email')]
        );

        $response = $this->actingAs($user)->get($verificationUrl);

        $response->assertStatus(403);
        $this->assertFalse($user->fresh()->hasVerifiedEmail());
    }

    /**
     * Asserts that users who are already verified are not able to be verified again.
     */
    public function test_it_does_not_verify_users_who_area_already_verified()
    {
        $user = User::factory()->create([
            'email_verified_at' => Date::now(),
        ]);

        $verificationUrl = URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(60),
            ['id' => $user->id, 'hash' => sha1($user->email)]
        );

        $response = $this->actingAs($user)->get($verificationUrl);

        $response->assertStatus(422);
        $response->assertJson(['status' => __('auth.email.verification.already-verified')]);
    }

    /**
     * Asserts that emails can indeed be verified upon valid requests.
     */
    public function test_it_successfully_verifies_the_user_with_a_valid_request()
    {
        Event::fake();

        $user = User::factory()->create([
            'email_verified_at' => null,
        ]);

        $verificationUrl = URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(60),
            ['id' => $user->id, 'hash' => sha1($user->email)]
        );

        $response = $this->actingAs($user)->get($verificationUrl);

        Event::assertDispatched(Verified::class);
        $this->assertTrue($user->fresh()->hasVerifiedEmail());
        $response->assertStatus(200);
        $response->assertJson(['status' => __('auth.email.verification.success')]);
    }
}
