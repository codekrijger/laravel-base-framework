<?php

namespace Authentication\Controllers;

use App\Authentication\Controllers\EmailVerificationResend;
use App\Models\User;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

/**
 * Tests the working of the {@see EmailVerificationResend} controller.
 */
class EmailVerificationResendTest extends TestCase
{
    /**
     * Asserts that the code does not send a verification email if the user is not logged in.
     * Instead it should trigger the {@see \App\Http\Middleware\Authenticate} middleware.
     */
    public function test_it_does_not_send_a_verification_email_if_the_user_is_not_logged_in()
    {
        $response = $this->post(route('verification.send'));

        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * Asserts that the code does not send a verification email for users that
     * are already verified.
     */
    public function test_it_does_not_send_a_verification_email_for_an_already_verified_user()
    {
        $user = User::factory()->create([
            'email_verified_at' => Date::now(),
        ]);
        $response = $this->actingAs($user)->post(route('verification.send'));

        $response->assertStatus(422);
        $response->assertJson(['status' => __('auth.email.verification.already-verified')]);
    }

    /**
     * Asserts that users with a not yet verified email address will be able to trigger
     * resending of the verification email.
     */
    public function test_it_sends_a_verification_email_for_a_valid_request()
    {
        Notification::fake();

        $user = User::factory()->create([
            'email_verified_at' => null,
        ]);
        $response = $this->actingAs($user)->post(route('verification.send'));

        $response->assertStatus(200);
        $response->assertJson(['status' => __('auth.email.verification.resend')]);
        Notification::assertSentTo($user, VerifyEmail::class);
    }
}
