<?php

namespace Tests\Feature\Authentication\Controllers;

use App\Authentication\Controllers\RequestToken;
use App\Models\User;
use Faker\Factory;
use Tests\TestCase;

/**
 * Tests the working of the {@see RequestToken} controller.
 */
class RequestTokenTest extends TestCase
{
    /**
     * This method tests whether or not users can obtain tokens given valid credentials.
     */
    public function test_it_returns_tokens_for_valid_users()
    {
        $user = User::factory()->create();

        $this->json('post', route('login'), [
            'email' => $user->email,
            'password' => 'password',
            'device_name' => 'Unittest runner'
        ])
            ->assertStatus(200)
            ->assertJsonStructure(['token']);
    }

    /**
     * This method tests whether or not users with an incorrect password are
     * being rightfully rejected in the controller.
     */
    public function test_it_rejects_users_with_invalid_credentials()
    {
        $user = User::factory()->create();

        $this->json('post', route('login'), [
            'email' => $user->email,
            'password' => 'wrong password',
            'device_name' => 'Unittest runner'
        ])
            ->assertStatus(401)
            ->assertJson(['errors' => [__('auth.failed')]]);
    }

    /**
     * This method tests whether or not the passed data is being properly validated.
     */
    public function test_it_blocks_requests_with_faulty_payloads()
    {
        $faker = Factory::create();

        // If everything is invalid, everything should show an error.
        $this->json('post', route('login'), [])
            ->assertStatus(400)
            ->assertJsonStructure(['email' => [], 'password' => [], 'device_name' => []]);

        // If one of the entries is valid, it should not show an error
        $this->json('post', route('login'), [
            'email' => $faker->email,
        ])
            ->assertStatus(400)
            ->assertJsonStructure(['password' => [], 'device_name' => []]);

        // In case the device_name is too long, it should still error
        $this->json('post', route('login'), [
            'device_name' => $faker->regexify('[A-Za-z0-9]{300}')
        ])
            ->assertStatus(400)
            ->assertJsonStructure(['password' => [], 'device_name' => []]);
    }
}
