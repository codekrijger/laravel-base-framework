<?php

namespace App\Providers;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        ResetPassword::createUrlUsing(function ($user, string $token) {
            // Since we don't have the reset-password url defined in the Laravel routing
            // (but instead it is being handled by React).
            // We have to define a custom handler to create a url that points to the React
            // page instead of the Laravel page.
            // See the comment below with regards to encoding the email.
            $encoded_email = base64_encode($user->email);
            return config('app.frontend_url') . '/reset-password?t=' . $token . '&e=' . $encoded_email;
        });

        VerifyEmail::createUrlUsing(function ($notifiable) {
            // See the doc in ResetPassword for an explanation about this custom handler.
            $backend_url = URL::temporarySignedRoute(
                'verification.verify',
                Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
                [
                    'id' => $notifiable->getKey(),
                    'hash' => sha1($notifiable->getEmailForVerification()),
                ]
            );

            $backend_url = str_replace(config('app.url'), '', $backend_url);
            // Encode the url with base64 to slightly increase the difficulty of snooping details.
            // Its not an issue if people try to decode this since:
            //  - You have to be logged in (with the correct user) in order to use the url
            //  - It contains another signature to sign the url
            //  - It is mainly here to prevent people from snooping details through the browser history
            $backend_url = base64_encode($backend_url);

            return config('app.frontend_url') . '/email-verification?t=' . $backend_url;
        });
    }
}
