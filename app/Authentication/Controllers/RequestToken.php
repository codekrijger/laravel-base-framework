<?php

namespace App\Authentication\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * The goal of this API controller is to handle the requestings of authentication tokens.
 */
class RequestToken extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function post(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|string|max:255',
            'device_name' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toArray(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()
                // Return errors as array to keep in line with errors being a list
                ->json(['errors' => [__('auth.failed')]], 401);
        }

        return response()
            ->json([
                'token' => $user->createToken($request->device_name)->plainTextToken,
                'is_verified' => $user->hasVerifiedEmail(),
            ], 200);
    }
}
