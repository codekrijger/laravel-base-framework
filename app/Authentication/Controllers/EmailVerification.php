<?php

namespace App\Authentication\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\JsonResponse;

/**
 * This controller handles verifications of users email addresses.
 * These urls are probably invoked through a link defined in the email
 * which has likely been send upon registration of the account.
 *
 * @link https://laravel.com/docs/8.x/verification
 */
class EmailVerification extends Controller
{
    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param EmailVerificationRequest $request
     * @return JsonResponse
     */
    public function __invoke(EmailVerificationRequest $request): JsonResponse
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json(['status' => __('auth.email.verification.already-verified')], 422);
        }

        $request->fulfill();

        return response()->json(['status' => __('auth.email.verification.success')], 200);
    }
}
