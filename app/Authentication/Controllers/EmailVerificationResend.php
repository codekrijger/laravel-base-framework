<?php

namespace App\Authentication\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * This class handles the resending of email verification emails.
 *
 * @link https://laravel.com/docs/8.x/verification#model-preparation
 */
class EmailVerificationResend extends Controller
{
    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json(['status' => __('auth.email.verification.already-verified')], 422);
        }

        $request->user()->sendEmailVerificationNotification();

        return response()->json(['status' => __('auth.email.verification.resend')], 200);
    }
}
