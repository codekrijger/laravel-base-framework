<?php

namespace App\Authentication\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * The goal of this controller is to facilitate the creation of accounts
 * for new users.
 */
class Registration extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function post(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|min:8|max:255',
            'device_name' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toArray(), 400);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        // https://laravel.com/docs/8.x/verification#model-preparation
        // We're sending this event to kick off the email-verification process once an user registers
        event(new Registered($user));

        return response()->json([
            'status' => __('auth.account.created'),
            'token' => $user->createToken($request->device_name)->plainTextToken,
            'is_verified' => $user->hasVerifiedEmail(),
        ], 200);
    }
}
