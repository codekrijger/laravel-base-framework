<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'account' => [
        'created' => 'Successfully created the account. Please check your email to verify your account!',
    ],
    'email' => [
        'verification' => [
            'resend' => 'Successfully resend the email verification email.',
            'success' => 'Successfully verified the email address.',
            'already-verified' => 'The given email address has already been verified.'
        ],
    ],
    'logged-out' => 'Successfully logged out',
];
