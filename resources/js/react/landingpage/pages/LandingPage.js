import React from "react";

function LandingPage(props) {
    return (
        <div className="flex justify-center items-center flex-grow bg-gray-100">
            <div className="container">
                <div className="bg-white rounded-lg shadow-lg p-5 md:p-20 mx-2">
                    <div className="text-center">
                        <h2 className="text-4xl tracking-tight leading-10 font-extrabold text-gray-900 sm:text-5xl sm:leading-none md:text-6xl">
                            { process.env.MIX_APP_NAME }
                        </h2>
                        <h3 className='text-xl md:text-3xl mt-10'>Coming Soon</h3>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LandingPage;
