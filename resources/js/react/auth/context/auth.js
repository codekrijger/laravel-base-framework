import { createContext, useContext } from 'react';

export const AuthContext = createContext(null);

/**
 * A simple context that can be used to store the authentication token
 * of the user.
 *
 * Based on the following tutorial:
 *
 * @link https://medium.com/better-programming/building-basic-react-authentication-e20a574d5e71
 */
export function useAuth() {
    return useContext(AuthContext);
}
