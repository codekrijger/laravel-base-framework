import React, {useState} from "react";
import Header from "../../core/components/Header";
import FormInput from "../../core/components/FormInput";
import Button from "../../core/components/Button";
import {faLock} from "@fortawesome/free-solid-svg-icons";
import ErrorList from "../../core/components/ErrorList";
import {toast} from "react-toastify";

function ForgotPassword() {
    const [email, setEmail] = useState("");
    const [errors, setErrors] = useState({});
    const [submitted, setSubmitted] = useState(false);

    function submitForgotPassword() {
        setSubmitted(true);

        axios
            .post("api/forgot-password", { email: email })
            .then(result => {
                setErrors({});
                toast.success(result.data.status);
            })
            .catch(e => {
                setErrors(e.response.data);
            })
            .finally(() => {
                setSubmitted(false);
            });
    }

    return (
        <>
            <div className="flex justify-center items-center flex-grow bg-gray-100 py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <Header label="Forgot password?" />
                        <p className="mt-2 text-center text-sm text-gray-600">
                            Fill in your e-mail and we'll e-mail you a link that allows you to reset your password! <br/>
                        </p>
                    </div>
                    <div className="mt-8 space-y-6">
                        <div className="rounded-md -space-y-px">
                            <FormInput id="email" name="email" type="email" label="Email address"
                                       placeholder="Email address" value={email} errors={errors.email}
                                       onChange={e => { setEmail(e.target.value); }}
                            />
                        </div>

                        <Button disabled={ submitted } label="Forgot password" icon={ faLock } onClick={ submitForgotPassword }/>
                        <ErrorList errors={ errors.errors } />
                    </div>
                </div>
            </div>
        </>
    );
}

export default ForgotPassword;
