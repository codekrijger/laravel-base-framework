import React, {useState} from "react";
import Button from "../../core/components/Button";
import Header from "../../core/components/Header";
import AppLink from "../../core/components/AppLink";
import FormInput from "../../core/components/FormInput";
import {faLock} from "@fortawesome/free-solid-svg-icons";
import ErrorList from "../../core/components/ErrorList";
import {toast} from "react-toastify";
import {useAuth} from "../context/auth";

function Register() {
    const [errors, setErrors] = useState({});
    const [submitted, setSubmitted] = useState(false);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const {setAuthToken} = useAuth();

    function register() {
        setSubmitted(true);

        axios.post("/api/register", {
            name: name, email: email, password: password, device_name: navigator.userAgent
        }).then(result => {
            // Clear all the filled in fields to prevent re-submits.
            setEmail("");
            setPassword("");
            setName("");

            setAuthToken(result.data.token);
            setErrors({});
            toast.success(result.data.status);
        }).catch(e => {
            setErrors(e.response.data);
        }).finally(() => {
            setSubmitted(false);
        });
    }

    return (
        <React.Fragment>
            <div className="flex justify-center items-center flex-grow bg-gray-100 py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <Header label="Create account" />
                    </div>
                    <div className="mt-8 space-y-6">
                        <div className="rounded-md -space-y-px">
                            <FormInput id="name" name="name" type="name" label="Name" groupLocation="first"
                                       placeholder="Name" value={name} errors={errors.name}
                                       onChange={e => { setName(e.target.value); }}
                            />
                            <FormInput id="email" name="email" type="email" label="Email address" groupLocation="middle"
                                       placeholder="Email address" value={email} errors={errors.email}
                                       onChange={e => { setEmail(e.target.value); }}
                            />
                            <FormInput id="password" name="password" type="password" label="Password" groupLocation="last"
                                       placeholder="Password" value={password} errors={errors.password}
                                       onChange={e => { setPassword(e.target.value); }}
                            />
                        </div>

                        <div className="flex items-center justify-end text-sm">
                            <AppLink to="/login" label="Already have an account?"/>
                        </div>

                        <Button disabled={ submitted } label="Register" icon={ faLock } onClick={ register }/>

                        <ErrorList errors={ errors.error } />
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Register;
