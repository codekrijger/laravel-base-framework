import React, {useState} from "react";
import {Redirect} from "react-router-dom";
import Header from "../../core/components/Header";
import Button from "../../core/components/Button";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";
import {toast} from "react-toastify";
import Base64Tools from "../../core/utils/Base64Tools";

function EmailVerification(props) {
    const encodedApiURL = new URLSearchParams(props.location.search).get("t");
    const [verificationStatus, setVerificationStatus] = useState('Verifying email.');
    const [submittedEmailVerification, setSubmittedEmailVerification] = useState(false);
    const [submittedResend, setSubmittedResend] = useState(false);
    const [verificationSuccessful, setVerificationSuccessful] = useState(false);

    function submitEmailVerification(decodedApiURL) {
        setSubmittedEmailVerification(true);

        axios
            .get(decodedApiURL)
            .then(result => {
                setVerificationStatus(result.data.status);
                toast.success(result.data.status);
                // If the email was successfully submitted, we're disabling the resend functionality.
                setSubmittedResend(true);
                // And last but not least we set the indicator that the verification was successful.
                // This to redirect the user to the dashboard area.
                setVerificationSuccessful(true);
            })
            .catch(e => {
                setVerificationStatus(e.response.data.status ?? 'Unable to verify email, please try again later.');
            });
    }

    function resendEmailVerification() {
        setSubmittedResend(true);

        axios
            .post("/api/email/verification/resend")
            .then(result => {
                toast.success(result.data.status);
            })
            .catch(e => {
                toast.error(e.response.data.status);
            })
            .finally(() => {
                setSubmittedResend(false);
            });
    }

    if (encodedApiURL && !submittedEmailVerification) {
        const decodedApiURL = Base64Tools.attemptDecodingString(encodedApiURL);

        if (decodedApiURL) {
            submitEmailVerification(decodedApiURL);
        } else {
            setVerificationStatus('Invalid token, please request a new verification e-mail.');
        }
    }

    if (submittedEmailVerification && verificationSuccessful) {
        return <Redirect to="/dashboard"/>;
    }

    return (
        <>
            <div className="flex justify-center items-center flex-grow bg-gray-100 py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <Header label="Verify your email" />
                    </div>
                    <div className="mt-8 space-y-6">
                        <p className="mt-2 text-center text-sm text-gray-600">
                            { encodedApiURL ?
                                verificationStatus :
                                'No email verification details set, please resend the email-verification.'
                            }
                        </p>
                        <Button disabled={ submittedResend } label="Resend email verification" icon={ faEnvelope } onClick={ resendEmailVerification }/>
                    </div>
                </div>
            </div>
        </>
    );
}

export default EmailVerification;
