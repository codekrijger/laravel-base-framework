import React, {useState} from 'react';
import {toast} from "react-toastify";
import {Redirect} from "react-router-dom";
import Header from "../../core/components/Header";
import FormInput from "../../core/components/FormInput";
import Button from "../../core/components/Button";
import {faLock} from "@fortawesome/free-solid-svg-icons";
import ErrorList from "../../core/components/ErrorList";
import Base64Tools from "../../core/utils/Base64Tools";

function ResetPassword(props) {
    const queryParams = new URLSearchParams(props.location.search);
    const token = queryParams.get("t");
    const encodedEmail = queryParams.get("e");
    const [email, setEmail] = useState(Base64Tools.attemptDecodingString(encodedEmail));
    const [newPassword, setNewPassword] = useState("")
    const [submitted, setSubmitted] = useState(false);
    const [errors, setErrors] = useState({});
    const [updatedPassword, setUpdatedPassword] = useState(false);

    function submitResetPassword() {
        setSubmitted(true);

        axios.post("/api/reset-password", {
            token: token, email: email, password: newPassword
        }).then(result => {
            setErrors({});
            toast.success(result.data.status);
            setUpdatedPassword(true);
        }).catch(e => {
            setErrors(e.response.data);
        }).finally(() => {
            setSubmitted(false);
        });
    }

    if (!token) {
        toast.error("No token set, please use the full link from the email.");
        return <Redirect to="/forgot-password"/>;
    }

    if (updatedPassword) {
        return <Redirect to="/login"/>;
    }

    return (
        <>
            <div className="flex justify-center items-center flex-grow bg-gray-100 py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <Header label="Reset password" />
                        <p className="mt-2 text-center text-sm text-gray-600">
                            Please enter your new password.
                        </p>
                    </div>
                    <div className="mt-8 space-y-6">
                        <div className="rounded-md -space-y-px">
                            <FormInput id="email" name="email" type="email" label="Email address" groupLocation="first"
                                       placeholder="Email address" value={email} errors={errors.email}
                                       onChange={e => { setEmail(e.target.value); }}
                            />
                            <FormInput id="newPassword" name="newPassword" type="password" label="New Password" groupLocation="last"
                                       placeholder="New Password" value={newPassword} errors={errors.password}
                                       onChange={e => { setNewPassword(e.target.value); }}
                            />
                        </div>

                        <Button disabled={ submitted } label="Forgot password" icon={ faLock } onClick={ submitResetPassword }/>
                        <ErrorList errors={ errors.errors } />
                    </div>
                </div>
            </div>
        </>
    );
}

export default ResetPassword;
