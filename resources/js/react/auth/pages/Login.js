import React, {useState} from "react";
import {Redirect} from "react-router-dom";
import Button from "../../core/components/Button";
import {useAuth} from "../context/auth";
import FormInput from "../../core/components/FormInput";
import {faLock} from '@fortawesome/free-solid-svg-icons'
import AppLink from "../../core/components/AppLink";
import Header from "../../core/components/Header";
import ErrorList from "../../core/components/ErrorList";
import {toast} from "react-toastify";

export default function Login(props) {
    const [isLoggedIn, setLoggedIn] = useState(false);
    const [errors, setErrors] = useState({});
    const [submitted, setSubmitted] = useState(false);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const {setAuthToken} = useAuth();
    const referer = props.location.state?.referer || '/dashboard';

    function login() {
        setSubmitted(true);

        axios.post("/api/login", {
            email: email, password: password, device_name: navigator.userAgent
        }).then(result => {
            setAuthToken(result.data.token);
            setErrors({});
            setLoggedIn(true);
            toast.info("Your email hasn't been verified yet, please verify your email before continuing.");
        }).catch(e => {
            setErrors(e.response.data);
            // We don't have to update submitted when someone logs in, this because
            // we will be redirecting them.
            setSubmitted(false);
        });
    }

    if (isLoggedIn) {
        return <Redirect to={referer}/>;
    }

    return (
        <>
            <div className="flex justify-center items-center flex-grow bg-gray-100 py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <Header label="Sign in to your account" />
                        <p className="mt-2 text-center text-sm text-gray-600">
                            Or&nbsp;
                            <AppLink to="/register" label="Create an account"/>
                        </p>
                    </div>
                    <div className="mt-8 space-y-6">
                        <div className="rounded-md -space-y-px">
                            <FormInput id="email" name="email" type="email" label="Email address" groupLocation="first"
                                       placeholder="Email address" value={email} errors={errors.email}
                                       onChange={e => { setEmail(e.target.value); }}
                            />
                            <FormInput id="password" name="password" type="password" label="Password" groupLocation="last"
                                       placeholder="Password" value={password} errors={errors.password}
                                       onChange={e => { setPassword(e.target.value); }}
                            />
                        </div>

                        <div className="flex items-center justify-end text-sm">
                            <AppLink to="/forgot-password" label="Forgot password?"/>
                        </div>

                        <Button disabled={ submitted } label="Log in" icon={ faLock } onClick={ login }/>

                        <ErrorList errors={ errors.errors } />
                    </div>
                </div>
            </div>
        </>
    );
}
