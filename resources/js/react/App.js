import React, { useState } from "react";
import ReactDOM from "react-dom";
import {ToastContainer} from "react-toastify";
import Routing from "./core/routing/Routing";

export default function App() {
    const existingTokens = JSON.parse(localStorage.getItem("token"));
    const [authToken, setAuthToken] = useState(existingTokens);

    const storeAuthToken = (data) => {
        localStorage.setItem("token", JSON.stringify(data))
        setAuthToken(data);

        // If we have a token, we will also update our interceptor to make use of this new
        // token with all the requests.
        axios.interceptors.request.use(function (config) {
            config.headers.Authorization = 'Bearer ' + data;
            return config;
        });
    }

    axios.defaults.baseURL = process.env.MIX_BACKEND_BASE_URL;
    axios.interceptors.request.use(function (config) {
        if (authToken) {
            config.headers.Authorization = 'Bearer ' + authToken;
        }
        config.headers.Accept = 'application/json';
        config.headers.ContentType = 'application/json';

        return config;
    });

    return (
        <>
            <ToastContainer />
            <Routing getAuthToken={ authToken } setAuthToken={ storeAuthToken } />
        </>
    )
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
