import React from "react";

function Dashboard(props) {
    return (
        <>
            <div className="flex justify-center items-center flex-grow bg-gray-100 py-12 px-4 sm:px-6 lg:px-8">
                Dashboard!
            </div>
        </>
    );
}

export default Dashboard;
