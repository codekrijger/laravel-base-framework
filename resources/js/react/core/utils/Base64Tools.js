export default class Base64Tools {
    /**
     * Checks whether or not the given string is a Base64 valid string.
     * It is not fully foolproof, but it will prevent decoding errors
     * when attempting to decode the given string.
     *
     * Obtained from:
     * @link https://stackoverflow.com/a/34068497
     *
     * @param {string} encodedString
     * @returns {boolean}
     */
    static isBase64(encodedString) {
        if (!encodedString || encodedString === '' || encodedString.trim() === '') {
            return false;
        }

        try {
            return btoa(atob(encodedString)) === encodedString;
        } catch (err) {
            return false;
        }
    }

    /**
     * Attempts to decode the given encoded string. If it is unable to do so, it will return
     * an empty string ('').
     *
     * @param {string} encodedString
     * @returns {string} Either the decoded string or an empty string on failure.
     */
    static attemptDecodingString(encodedString) {
        if (Base64Tools.isBase64(encodedString)) {
            return atob(encodedString);
        }

        return '';
    }
}
