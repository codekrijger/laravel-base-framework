import React from 'react';
import ErrorList from "./ErrorList";

const FormInput = (props) => {
    let roundedClass;
    let borderClass = props.errors ? "border-red-400" : "border-gray-300";

    if (props.groupLocation === "first") {
        roundedClass = "rounded-t-md";
    } else if (props.groupLocation === "last") {
        roundedClass = "rounded-b-md";
    }

    return (
        <>
            <label htmlFor={ props.id } className="sr-only">
                { props.label }
            </label>
            <input id={ props.id } name={ props.name } type={ props.type } required
                   placeholder={ props.placeholder } value={ props.value } onChange={ props.onChange }
                   className={"shadow-sm appearance-none rounded-none relative block w-full px-3 py-2 border " + borderClass + " placeholder-gray-500 text-gray-900 " + roundedClass + " focus:outline-none focus:ring-primary-500 focus:border-primary-500 focus:z-10 sm:text-sm"} />
            <ErrorList errors={ props.errors } />
        </>
    )
};

export default FormInput;
