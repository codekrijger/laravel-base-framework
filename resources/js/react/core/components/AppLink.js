import React from 'react';
import {Link} from "react-router-dom";

const AppLink = (props) => {
    return (
        <Link to={ props.to } className="font-medium text-primary-600 hover:text-primary-500">
            { props.label }
        </Link>
    );
};

export default AppLink;
