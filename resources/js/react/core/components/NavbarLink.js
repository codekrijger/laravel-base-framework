import React from 'react';
import {NavLink} from "react-router-dom";

export const NavbarLink = (props) => {
    return (
        <>
            <NavLink
                to={ props.to } exact
                className="border-b hover:border-primary-600 hover:text-primary-600 text-gray-600 px-3 py-2 rounded-sm font-medium border-transparent"
                activeClassName="border-primary-600"
            >
                { props.label }
            </NavLink>
        </>
    );
}
