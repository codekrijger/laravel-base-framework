import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Button = (props) => {
    const buttonColorClasses = props.disabled ? "bg-primary-200 hover:bg-primary-400" : "bg-primary-600 hover:bg-primary-700";
    const iconColorClasses = props.disabled ? "text-primary-100 group-hover:text-primary-300" : "text-primary-500 group-hover:text-primary-400";

    const buttonIcon = props.icon ? (
        <span className="absolute left-0 inset-y-0 flex items-center pl-3">
            <FontAwesomeIcon
                className={ "h-5 w-5 " + iconColorClasses }
                icon={props.icon}
            />
        </span>
    ) : null;

    return (
        <button type="button"
                onClick={ props.onClick }
                disabled={ props.disabled }
                className={ "group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white " + buttonColorClasses + " focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500" }>
            { buttonIcon }
            { props.label }
        </button>
    );
};

export default Button;
