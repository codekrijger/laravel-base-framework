import React from "react";
import {NavbarLink} from "./NavbarLink";

export default function Navbar() {
    return (
        <>
            <nav className="bg-secondary-600">
                <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
                    <div className="relative flex items-center justify-between h-16">
                        <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                            <div className="flex-shrink-0 flex items-center">
                                <h2>{ process.env.MIX_APP_NAME }</h2>
                            </div>
                            <div className="hidden sm:block sm:ml-6">
                                <div className="flex space-x-4">
                                    <NavbarLink to="/" label="Home"/>
                                </div>
                            </div>
                        </div>
                        <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                            <div className="flex space-x-4">
                                <NavbarLink to="/login" label="Sign in"/>
                                <NavbarLink to="/register" label="Register"/>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </>
    );
}
