import React from 'react';

const ErrorList = (props) => {
    let errors = null;

    if (props.errors) {
        errors = (
            <ol className="list-dash items-center font-medium text-red-500 text-xs mt-1 mb-1 ml-3">
                { props.errors.map((item) => <li key={ item }>{ item }</li>) }
            </ol>
        )
    }

    return (
        <>
            { errors }
        </>
    )
};

export default ErrorList;
