import React from 'react';

const Header = (props) => {
    return (
        <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
            { props.label }
        </h2>
    );
};

export default Header;
