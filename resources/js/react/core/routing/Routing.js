import React  from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PrivateRoute from './../../core/routing/PrivateRoute';
import { AuthContext } from "../../auth/context/auth";
import Register from "../../auth/pages/Register";
import Login from "../../auth/pages/Login";
import Dashboard from "../../dashboard/pages/Dashboard";
import LandingPage from "../../landingpage/pages/LandingPage";
import NotFound from "./../../core/pages/NotFound";
import EmailVerification from "../../auth/pages/EmailVerification";
import ForgotPassword from "../../auth/pages/ForgotPassword";
import ResetPassword from "../../auth/pages/ResetPassword";
import Navbar from "../components/Navbar";

export default function Routing(props) {
    return (
        <>
            <AuthContext.Provider value={{ authToken: props.getAuthToken, setAuthToken: props.setAuthToken }}>
                <Router>
                    <Navbar />
                    <Switch>
                        <Route exact path="/" component={LandingPage} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/forgot-password" component={ForgotPassword} />
                        <Route exact path="/reset-password" component={ResetPassword} />
                        <PrivateRoute exact path="/email-verification" component={EmailVerification} />
                        <PrivateRoute exact path="/dashboard" component={Dashboard} />
                        <Route component={NotFound} />
                    </Switch>
                </Router>
            </AuthContext.Provider>
        </>
    )
}
