# ReadLater

## Run composer

docker run --rm --interactive --tty --volume $PWD:/app --volume ${COMPOSER_HOME:-$HOME/.composer}:/tmp composer/composer create-project laravel/laravel readlater

## Getting started

We're running everything through [Laravel sail](https://laravel.com/docs/8.x/sail).

Please follow the following steps to get started:

1. `docker run --rm --interactive --tty --volume $PWD:/app --volume ${COMPOSER_HOME:-$HOME/.composer}:/tmp composer/composer install`
2. `cp .env.example .env`
3. Modify the `.env` file to your liking
4. `php artisan sail:install`
5. `alias sail='bash vendor/bin/sail'` -> Add this to your bash file (or e.g. `~/.zshrc`) and then `source` that file to reload it.
6. `sail up -d`
7. `sail artisan migrate`
8. `sail npm install && sail npm run dev`
9. `sail artisan serve` 

## Running tests

To run the PHP / Laravel tests, you can execute the following command:

- `sail test`

Any [CLI command](https://laravel.com/docs/8.x/testing#artisan-test-runner) that is available, can also be passed to this command (e.g. `--testsuite=Feature --stop-on-failure`)
