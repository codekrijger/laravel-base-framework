const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// See: https://dev.to/klickers/setting-up-tailwindcss-with-sass-in-laravel-2p2d
// For the details about intergrating tailwindcss with sass
const tailwindcss = require('tailwindcss');

mix
    .react('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('./tailwind.config.js') ],
    });

// See: https://laravel.com/docs/8.x/mix#versioning-and-cache-busting
// Adds a unique hash to the filename to prevent the browser from caching it upon releases
// Only doing this in production right now since this is very likely not helpful for development.
if (mix.inProduction()) {
    mix.version();
}
